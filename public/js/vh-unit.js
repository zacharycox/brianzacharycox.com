// NOTE:
// -Using escape hatch (this file is outside of module system)
// -Do NOT follow Gatsby site instruction to add pathPrefix to gatsby-config (https://www.gatsbyjs.org/docs/static-folder/)
//  -pathPrefix seems to be generated automatically for files in the root 'static' folder
//  -defining pathPrefix in gatsby-config appears to overwrite other definitions,
//    rather than being merged into webpack config

// REF:
// https://css-tricks.com/the-trick-to-viewport-units-on-mobile/

window.addEventListener('resize', throttle(VhUnit, 250));

function throttle(fn, threshhold, scope) {
  threshhold || (threshhold = 250);
  var last,
    deferTimer;
  return function () {
    var context = scope || this;

    var now = +new Date,
      args = arguments;
    if (last && now < last + threshhold) {
      // hold on to it
      clearTimeout(deferTimer);
      deferTimer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshhold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
}

function VhUnit() {
  // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
  let vh = window.innerHeight * 0.01;
  // Then we set the value in the --vh custom property to the root of the document
  document.documentElement.style.setProperty('--vh', `${vh}px`);
}

VhUnit();