import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';
import FeatureButton from '../components/feature-button.js';
import TypedSpan from '../components/typed-span.js';

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={['gatsby', 'application', 'react']} />
    <section className={'container'}>
      <section className={'atf'}>
        <div className={'atf__content-wrap'}>
          <h1 className={'atf__hdg'}><TypedSpan text={['frontend', 'ui.ux', 'javascript', 'zacharycox']} />.developer</h1>
          <div className={'atf__feature-buttons-wrap'}>
            <FeatureButton text={'Contact'} action={'/contact/'} size={'medium'}/>
            <FeatureButton text={'Resume'} action={'/resume/'} size={'medium'} />
          </div>
        </div>
      </section>
    </section>
  </Layout>
)

export default IndexPage
