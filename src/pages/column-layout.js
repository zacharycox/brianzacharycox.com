import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';

const ColumnLayout = () => (
  <Layout>
    <SEO title="Column Layout" keywords={['gatsby', 'application', 'react']} />
    <section className={'container'}>
      <div className={'columns'}>
        <div className={'column-1-2'}>1-2</div>
        <div className={'column-2-2'}>2-2</div>
      </div>
      <div className={'columns'}>
        <div className={'column-1-3'}>1-3</div>
        <div className={'column-2-3'}>2-3</div>
      </div>
      <div className={'columns'}>
        <div className={'column-3-3'}>3-3</div>
      </div>
      <div className={'columns'}>
        <div className={'column-1-4'}>1-4</div>
        <div className={'column-1-4'}>1-4</div>
        <div className={'column-2-4'}>2-4</div>
      </div>
      <div className={'columns'}>
        <div className={'column-1-4'}>1-4</div>
        <div className={'column-3-4'}>3-4</div>
      </div>
      <div className={'columns'}>
        <div className={'column-4-4'}>4-4</div>
      </div>
      <div className={'columns'}>
        <div className={'column-1-5'}>1-5</div>
        <div className={'column-2-5'}>2-5</div>
        <div className={'column-2-5'}>2-5</div>
      </div>
      <div className={'columns'}>
        <div className={'column-2-5'}>2-5</div>
        <div className={'column-2-5'}>3-5</div>
      </div>
    </section>
  </Layout>
)

export default ColumnLayout
