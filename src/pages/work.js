import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';
import HC from '../images/work/hc-d-2.jpg';
import Speidel from '../images/work/speidel-d-2.jpg';
import Munchpak from '../images/work/munchpak-d-2.jpg';
import Daron from '../images/work/daron-d-2.jpg';
import Naturebox from '../images/work/naturebox-d-3.jpg';
import Ripen from '../images/work/ripen-logo-blk.svg';
import Lifetrak from '../images/work/lifetrak-d-2.jpg';
import EmbroideryAnimation from '../assets/hc-embroidery-animation.webm';
import Templates from '../images/work/templates.jpg';
import WorkflowAnimated from '../images/work/workflow-animated.gif';
import Workflow from '../images/work/workflow.jpg';
import Widgets from '../images/work/widgets.gif';
import GearLg from '../images/work/gear-lg.png';
import GearSm from '../images/work/gear-sm.png';
import Templates1 from '../images/work/templates-1.jpg'
import Templates2 from '../images/work/templates-2.jpg'
import Templates3 from '../images/work/templates-3.jpg'
import Templates4 from '../images/work/templates-4.jpg'
import Templates5 from '../images/work/templates-5.jpg'
import Templates6 from '../images/work/templates-6.jpg'

class Work extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      animations: [
        {
          id: 'efficient__img-stat',
        },
        {
          id: 'efficient__img-templates',
        },
        {
          id: 'efficient__img-widgets',
        },
        {
          id: 'efficient__img-workflow',
        },
      ],
      activeThings: [],
    };

    this.rootAnimationRef = React.createRef();

    this.animationRefs = this.state.animations.reduce((acc, value) => {
      acc[value.id] = {
        ref: React.createRef(),
        id: value.id,
        ratio: 0,
      };
      return acc;
    }, {});

    this.isActive = this.isActive.bind(this);
  }

  isActive(animationId) {
    let result = false;
    this.state.activeThings.forEach((id) => {
      if(id === animationId) {
        result = true;
        return result;
      }
    });
    return result;
  };

  componentDidMount() {
    const callback = entries => {
      entries.forEach(
        entry =>
          (this.animationRefs[entry.target.id].ratio = entry.intersectionRatio),
      );

      const activeThings = [];

      for(let animationRef in this.animationRefs) {
        if(this.animationRefs.hasOwnProperty(animationRef)) {
          if (this.animationRefs[animationRef]['ratio'] >= 1) {
            activeThings.push(this.animationRefs[animationRef]['id']);
          }
        }
      }

      if (activeThings) {
        this.setState({ activeThings: activeThings });
      }
    };
    
    this.observer = new IntersectionObserver(callback, {
      root: this.rootAnimationRef.current,
      threshold: new Array(101).fill(0).map((v, i) => i * 0.01),
    });

    Object.values(this.animationRefs).forEach(value =>
      this.observer.observe(value.ref.current),
    );
  }

  render() {
    const transDur1 = {transitionDelay: '0ms'}
    const transDur2 = {transitionDelay: '500ms'}
    const transDur3 = {transitionDelay: '1000ms'}
    const transDur4 = {transitionDelay: '1500ms'}
    const transDur5 = {transitionDelay: '2000ms'}
    const transDur6 = {transitionDelay: '2500ms'}

    const dur1 = this.isActive('efficient__img-templates') ? transDur1 : transDur6;
    const dur2 = this.isActive('efficient__img-templates') ? transDur2 : transDur5;
    const dur3 = this.isActive('efficient__img-templates') ? transDur3 : transDur4;
    const dur4 = this.isActive('efficient__img-templates') ? transDur4 : transDur3;
    const dur5 = this.isActive('efficient__img-templates') ? transDur5 : transDur2;
    const dur6 = this.isActive('efficient__img-templates') ? transDur6 : transDur1;

    return (
      <Layout>
        <SEO title="Work" keywords={['work', 'Brian Zachary Cox', 'JavaScript Developer']} />
        <section className={'container--centered'}>
          <h1 className={'page__heading'}>my work</h1>
        </section>
        <section className={'page__section tiles'}>
          <div className={'section__heading container--centered'}>
            <h2>responsive</h2>
          </div>
          <div className={'columns'}>
            <div className={'column-4-10 tiles__column-left'}>
              <img src={HC} alt={'Happy Chef\u00AE'} title={'Happy Chef\u00AE'}/>
              <div className={'columns'}>
                <div className={'column-1-2'}>
                  <img src={Daron} alt={'Daron'} title={'Daron'}/>
                </div>
                <div className={'column-1-2 tiles__ripen'}>
                  <img src={Ripen} alt={'Ripen eCommerce'} title={'Ripen eCommerce'}/>
                </div>
              </div>
            </div>
            <div className={'column-6-10'}>
              <div className={'columns'}>
                <div className={'column-3-8'}>
                  <img src={Speidel} alt={'Speidel'} title={'Speidel'}/>
                </div>
                <div className={'column-5-8'}>
                  <img src={Munchpak} alt={'Munchpak'} title={'Munchpak'}/>
                </div>
              </div>
              <div className={'columns'}>
                <div className={'column-6-10 tiles__naturebox-2'}>
                  <img src={Naturebox} alt={'Naturebox'} title={'Naturebox'}/>
                </div>
                <div className={'column-4-10 tiles__lifetrak'}>
                  <img src={Lifetrak} alt={'Lifetrak'} title={'Lifetrak'}/>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className={'page__section'}>
          <div className={'section__heading container--centered'}>
            <h2>interactive</h2>
          </div>
          <div className={'section__content'}>
            <video id={'hcEmbroideryAnimation'} autoPlay={'autoplay'} muted={true} loop>
              <source type={'video/webm'} src={EmbroideryAnimation}/>
            </video>
          </div>
        </section>
        <section className={'page__section efficient'}>
          <div className={'section__heading container--centered'}>
            <h2>efficient</h2>
            <p className={'section__blurb'}>For complex, iterative tasks that have to be done right, intuitive tools and efficient workflows are worth their weight in gold.</p>
          </div>
          <div className={'section__content--tall container--small'}>
            <div className={'split--left'}>
              <div id={'efficient__img-stat'} ref={this.animationRefs['efficient__img-stat'].ref} className={this.isActive('efficient__img-stat') ? 'split__image container--centered is-active' : 'split__image container--centered'}>
                <span className={'stat--lg'}>50<sup>%</sup></span>
                <div className={'stat__caption'}>
                  <span>reduction in development and deployment time</span>
                </div>
              </div>
              <div className={'split__text'}>
                <h3>Streamlining the process</h3>
                <p>With so many constantly-changing assets to manage, keeping an e-commerce site up to date can easily become a productivity bottleneck. For Happy Chef&reg;, I knew there had to be a better way than our current laborious, time-consuming process of site updates. The solution I came up with was a system of HTML templates, widgets, and a standardized workflow that cut development and deployment time in half.</p>
              </div>
            </div>
          </div>
          <div className={'section__content--tall container--small'}>
            <div className={'split--right'}>
              <div id={'efficient__img-templates'} ref={this.animationRefs['efficient__img-templates'].ref} className={this.isActive('efficient__img-templates') ? 'split__image container--centered is-active' : 'split__image container--centered'}>
                <div className={'template__scene'}>
                  <div className={'template__panel-wrap-1'}>
                    <div className={'template__panel'} style={dur1}>
                      <img src={Templates1} alt={'Templates'} />
                    </div>
                  </div>
                  <div className={'template__panel-wrap-2'}>
                    <div className={'template__panel'} style={dur2}>
                      <img src={Templates2} alt={'Templates'} />
                    </div>
                  </div>
                  <div className={'template__panel-wrap-3'}>
                    <div className={'template__panel'} style={dur3}>
                      <img src={Templates3} alt={'Templates'} />
                    </div>
                  </div>
                  <div className={'template__panel-wrap-4'}>
                    <div className={'template__panel'} style={dur4}>
                      <img src={Templates4} alt={'Templates'} />
                    </div>
                  </div>
                  <div className={'template__panel-wrap-5'}>
                    <div className={'template__panel'} style={dur5}>
                      <img src={Templates5} alt={'Templates'} />
                    </div>
                  </div>
                  <div className={'template__panel-wrap-6'}>
                    <div className={'template__panel'} style={dur6}>
                      <img src={Templates6} alt={'Templates'} />
                    </div>
                  </div>
                </div>
              </div>
              <div className={'split__text'}>
                <h3>Fast, reliable development</h3>
                <p>Using templates assured rapid, repeatable implementation of new assets. No more broken HTML or JavaScript, and no need to reinvent the wheel for each new sales promotion. A simple naming convention eliminated any confusion or ambiguity over which assets to deploy, and when.</p>
              </div>
            </div>
          </div>
          <div className={'section__content--tall container--small'}>
            <div className={'split--left'}>
              <div id={'efficient__img-widgets'} ref={this.animationRefs['efficient__img-widgets'].ref} className={this.isActive('efficient__img-widgets') ? 'split__image container--centered is-active' : 'split__image container--centered'}>
                <div className={'gears'}>
                  <img className={'gears--lg gears__0'} src={GearLg} alt={'Widgets'} />
                  <img className={'gears--sm gears__1'} src={GearSm} alt={'Widgets'} />
                  <img className={'gears--sm gears__2'} src={GearSm} alt={'Widgets'} />
                  <img className={'gears--sm gears__3'} src={GearSm} alt={'Widgets'} />
                </div>
              </div>
              <div className={'split__text'}>
                <h3>Quicker deployments</h3>
                <p>Deploying new assets via widgets allowed for seamless transition and a quick way to return the site to its previous state. On the off chance that the deployment went awry, or if the marketing team changed its plans, disruption to the site would be minimized (it's always good to have a backup plan).</p>
              </div>
            </div>
          </div>
          <div className={'section__content--tall container--small'}>
            <div className={'split--right'}>
              <div id={'efficient__img-workflow'} ref={this.animationRefs['efficient__img-workflow'].ref} className={this.isActive('efficient__img-workflow') ? 'split__image container--centered is-active' : 'split__image container--centered'}>
                <img src={this.isActive('efficient__img-workflow') ? WorkflowAnimated : Workflow} alt={'Workflow'} />
              </div>
              <div className={'split__text'}>
                <h3>Standardized workflow</h3>
                <p>An easy-to-follow, well-documented workflow meant that any developer could perform an update, even if they had never done one before. And if the task was reassigned mid-stream, a new developer could easily pick up where it had been left off.</p>
              </div>
            </div>
          </div>
        </section>
      </Layout>
    );
  }
}

export default Work