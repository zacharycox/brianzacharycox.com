import React from 'react';
import Layout from '../components/layout';
import SEO from "../components/seo";

const Blog = () => (
  <Layout>
    <section>
      <h1>Hello from the blog landing page</h1>
    </section>
    <SEO title="Blog" keywords={['gatsby', 'application', 'react']} />
  </Layout>
)

export default Blog
