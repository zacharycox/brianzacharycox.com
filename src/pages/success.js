import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';
import { Location } from '@reach/router';
import queryString from 'query-string';
import PropTypes from 'prop-types';

const Success = ContactorComponent => props => (
  <Layout>
    <SEO title="Contact" keywords={['contact', 'Brian Zachary Cox', 'Web Developer']} />
    <section className={'container'}>
      <div className={'page__heading'}>
        <h2>Thanks for reaching out,&nbsp;
          <Location>
            {({ location, navigate }) => (
              <ContactorComponent
                {...props}
                location={location}
                navigate={navigate}
                search={location.search ? queryString.parse(location.search) : {}}
              />
            )}
          </Location>
        !</h2>
        <p className={'section__blurb'}>I'll get back to you soon.</p>
      </div>
    </section>
  </Layout>
);

const ContactorFname = ({ search }) => {
  const {firstName} = search;
  return <span>{firstName}</span>
};

ContactorFname.propTypes = {
  search: PropTypes.object,
};

export default Success(ContactorFname)

// export default Success