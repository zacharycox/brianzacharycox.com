import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';
import ContactForm from "../components/contact-form";

const Contact = () => (
  <Layout>
    <SEO title="Contact" keywords={['contact', 'Brian Zachary Cox', 'Web Developer']} />
    <section className={'container'}>
      <section>
        <h1 className={'page__heading'}>contact me</h1>
      </section>
      <section>
        <ContactForm />
      </section>
    </section>
  </Layout>
)

export default Contact