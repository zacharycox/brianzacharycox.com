import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';
import ResumeContent from 'html-loader!../assets/resume-brian-zachary-cox.html';
import FeatureButton from "../components/feature-button";
import EmbroideryAnimation from "../assets/hc-embroidery-animation.webm";
import IconChrome from "../images/resume/chrome-512.png";
import IconSafari from "../images/resume/safari-280.png";
import IconFirefox from "../images/resume/firefox-300.png";
import IconXcode from "../images/resume/xcode-300.png";
import IconPhpStorm from "../images/resume/phpstorm-300.png";
import IconSequelPro from "../images/resume/sequelpro-300.png";
import IconGit from "../images/resume/git-300.png";
import IconBitbucket from "../images/resume/bitbucket-300.png";
import IconJira from "../images/resume/jira-300.png";
import IconPhotoshop from "../images/resume/photoshop-300.png";

const Resume = () => (
  <Layout>
    <SEO title="Resume" keywords={['resume', 'Brian Zachary Cox', 'JavaScript Developer']} />
      <section>
        <h1 className={'page__heading'}>my resume</h1>
      </section>
      <section className={'page__section resume container'}>
        <div className={'section__heading'}>
          <h2>the nitty gritty</h2>
        </div>
        <div className={'section__content'}>
          <div className={'resume__body'} dangerouslySetInnerHTML={{__html: ResumeContent}} />
          <FeatureButton
            cssClass={'content-cta'}
            text={'Download my resume'}
            actionType={'extnav'}
            action={'https://drive.google.com/open?id=1CG3mjTINgUrxrB-BfhClXlZl2F6_Rb6D8okrKQUdfCM'}
            size={'medium'}
            target={'new'}
          />
        </div>
      </section>
      <section className={'page__section tools has-bg container'}>
        <div className={'section__heading--short'}>
          <h2>my toolbox</h2>
        </div>
        <div className={'section__content'}>
          <div className={'columns tools__gallery'}>
            <div className={'column-1-5 tools__icon-wrap'}>
              <img className={'icon__devtools'} src={IconChrome} title={'Chrome DevTools'} alt={'Chrome DevTools'}/>
            </div>
            <div className={'column-1-5 tools__icon-wrap'}>
              <img className={'icon__devtools'} src={IconSafari} title={'Safari Web Inspector'} alt={'Safari Web Inspector'}/>
            </div>
            <div className={'column-1-5 tools__icon-wrap'}>
              <img className={'icon__devtools'} src={IconFirefox} title={'Mozilla Firefox'} alt={'Mozilla Firefox'}/>
            </div>
            <div className={'column-1-5 tools__icon-wrap'}>
              <img className={'icon__devtools'} src={IconXcode} title={'Xcode Simulator'} alt={'Xcode Simulator'}/>
            </div>
            <div className={'column-1-5 tools__icon-wrap'}>
              <img className={'icon__devtools'} src={IconPhpStorm} title={'JetBrains PhpStorm'} alt={'JetBrains PhpStorm'}/>
            </div>
            <div className={'column-1-5 tools__icon-wrap'}>
              <img className={'icon__devtools'} src={IconSequelPro} title={'Sequel Pro'} alt={'Sequel Pro'}/>
            </div>
            <div className={'column-1-5 tools__icon-wrap'}>
              <img className={'icon__devtools'} src={IconGit} title={'Git'} alt={'Git'}/>
            </div>
            <div className={'column-1-5 tools__icon-wrap'}>
              <img className={'icon__devtools'} src={IconBitbucket} title={'Bitbucket'} alt={'Bitbucket'}/>
            </div>
            <div className={'column-1-5 tools__icon-wrap'}>
              <img className={'icon__devtools'} src={IconJira} title={'Jira'} alt={'Jira'}/>
            </div>
            <div className={'column-1-5 tools__icon-wrap'}>
              <img className={'icon__devtools'} src={IconPhotoshop} title={'Adobe Photoshop'} alt={'Adobe Photoshop'}/>
            </div>
          </div>
        </div>
      </section>
      <section className={'page__section references container--small'}>
        <div className={'section__heading'}>
          <h2>recommendations</h2>
          <p className={'section__blurb'}>Nice things people have said about me (the "Bad things they said" page is still under construction).</p>
        </div>
        <div className={'section__content'}>
          <div className={'columns references__row'}>
            <div className={'column-1-2 reference'}>
              <blockquote className={'reference__comment'}>Zack is a highly dedicated developer who was a major contributor to our team's success. He's the kind of developer you can give a task to and count on it being done. He is also continually focused on growing his skills further. In the three years he was on my team he grew from being a junior developer to nearly a senior developer, gaining both depth and breadth of skills. I could count on him to work on any front-end task we had, and his familiarity with server-side code (PHP) let him work to a solution even if the roots of an issue or feature led him to that side. Any team that he joins will be glad to have him there.</blockquote>
              <div className={'reference__attribution'}>
                <div className={'reference__name'}>Scott Buchanan</div>
                <div className={'reference__title'}>Director of Engineering</div>
                <div className={'reference__company'}>RIPEN</div>
              </div>
            </div>
            <div className={'column-1-2 reference'}>
              <blockquote className={'reference__comment'}>Zach's attention to detail makes his front end development skills a natural fit with creative teams. In addition to being tactically skilled, Zach is also the kind of employee that performs his own sanity check on direction he's given and will proactively raise concerns or conflicts that might occur if he were to perform his task to the letter.<br /><br />I've always found Zach both hard working and easy to get along with. Any organization lucky enough to have him will find him skilled, dedicated, and a welcome addition to any team.</blockquote>
              <div className={'reference__attribution'}>
                <div className={'reference__name'}>David Rekuc</div>
                <div className={'reference__title'}>Director of Marketing</div>
                <div className={'reference__company'}>RIPEN</div>
              </div>
            </div>
          </div>
          <div className={'columns references__row'}>
            <div className={'column-1-2 reference'}>
              <blockquote className={'reference__comment'}>Zack makes UI development look easy….but... we all know it is not. I have worked closely with Zack on a series of demanding website development projects. Zack has always provided “heads-down, get’ er done” UI programming excellence on every project we have worked on together. If an update is needed quickly he will go the distance to complete the project within the deadline. If it’s a new technique, he picks it up very quickly and very often improves on the existing solution. Zack has a good eye for user experience and regularly provides salient improvements to designs in progress. Zack communicates effectively.<br /><br />As a project manager, I have always been able to count on him to seek clarification, if required, advise me of status on his tasks and work with me to ensure accurate formatting, proper responsive design and fully tested, effective functionality across platforms. I highly recommend Zack for any creative website development team where dedication, effective UI development and quality output is tantamount to company success.</blockquote>
              <div className={'reference__attribution'}>
                <div className={'reference__name'}>Tom Beers</div>
                <div className={'reference__title'}>Project Manager</div>
              </div>
            </div>
            <div className={'column-1-2 reference'}>
              <blockquote className={'reference__comment'}>Brian (Zach) is a unique talent. Not just due to his strong development skills, but also in his amazing work ethic, and eagerness to continually push himself to tackle development challenges (and doing so with clever solutions that are impressive). His initiative and self-sufficient approach to researching (when the answer is not already in hand) gave a very welcome sense of "Zach is on it, it's going to be great". Add to that his friendly personality in the agency office...... perfect example of who any agency leader would be grateful to have on their team.</blockquote>
              <div className={'reference__attribution'}>
                <div className={'reference__name'}>David Barbella</div>
                <div className={'reference__title'}>Full-Service Agency Agile Leadership</div>
              </div>
            </div>
          </div>
        </div>
      </section>
  </Layout>
)

export default Resume
