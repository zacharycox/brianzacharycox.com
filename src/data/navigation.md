---

navItems:
  - name: resume
    link: /resume
  - name: work
    link: /work
  - name: about
    submenu:
      - name: link 1
        link: /index
      - name: link 2
        link: /index

panels:
  - title: main
    level: 0
    items:
      - name: resume
        link: /resume
      - name: work
        link: /work
      - name: about
        target: about
  - title: about
    level: 1
    parent: main
    items:
      - name: link 1
        link: /index
      - name: link 2
        link: /index
    
---