import React from 'react';
import PropTypes from 'prop-types';

const Footer = ({ contentVar }) => (
    <footer className={'page__footer'}>
        <div className={"container"}>&copy; 2019 Brian Cox</div>
    </footer>
)

export default Footer
