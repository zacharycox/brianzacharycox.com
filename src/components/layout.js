import React from 'react';
import PropTypes from 'prop-types';
import {StaticQuery, graphql} from 'gatsby';
import {Helmet} from "react-helmet";
import { withPrefix } from 'gatsby';

import FixedHeader from "./fixed-header";
import Footer from './footer';
import '../styles/main.scss';

// wontdo: use context to get query data in nav.js directly
// see https://stackoverflow.com/questions/48157223/how-to-pass-information-to-nested-components-in-react
// todo: try to get StaticQuery working in Nav class
const Layout = ({children}) => (
    <StaticQuery
      query = {graphql `{
        allFile(
          filter: {
              internal: { mediaType: { eq: "text/markdown" } }
              name: { eq: "navigation" }
          }
        ) {
            edges {
              node {
                childMarkdownRemark {
                  frontmatter {
                    navItems {
                      name
                      link
                      submenu {
                        name
                        link
                      }
                    }
                    panels {
                      title
                      level
                      parent
                      items {
                        name
                        link
                        target
                      }
                    }
                  }
                }
              }
            }
          }
        }`
      }

      render = {data => (
          <>
            <Helmet>
              <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto|Heebo:100,300,400,500,700,800,900" rel="stylesheet" />
              <script src={withPrefix('/js/vh-unit.js')}></script>
            </Helmet>
            <FixedHeader navData={data}/>
            <main>
              { children }
            </main>
            <Footer />
          </>
      )}
    />
)

Layout.propTypes = {
    children: PropTypes.node.isRequired,
}

export default Layout
