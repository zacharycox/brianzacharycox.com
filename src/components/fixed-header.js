// Fixedheader component
import React from 'react';
import Link from 'gatsby-link';
import Logo from '../images/bzc-logo-11.svg';
import Nav from './nav';

class FixedHeader extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <header id={'fixedHeader'} className={'fixedhdr'}>
          <div className={'header__wrap container'}>
            <div className={'header--lt'}>
              <Link className={'logo'} to={'/'}>
                <div className={'logo__img-wrap'}>
                  <img className={'logo__img'} src={Logo} alt={'Developer'} />
                </div>
              </Link>
            </div>
            <div className={'header--rt'}>
              <Nav navData={this.props.navData}/>
            </div>
          </div>
        </header>
      </>
    );
  }
}

export default FixedHeader;
