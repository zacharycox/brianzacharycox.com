import React from 'react';
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types';
import Typed from 'typed.js';

class TypedSpan extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <span/>;
  }

  componentDidMount() {
    // find the DOM node for this component
    const node = ReactDOM.findDOMNode(this);

    let typed = new Typed(node, { 
      strings: this.props.text,
      typeSpeed: 100
    });

    // start a new React render tree with widget node
    ReactDOM.render(<span>{typed.toString()}</span>, node);
  }
}

TypedSpan.propTypes = {
  text: PropTypes.array,
}

TypedSpan.defaultProps = {
  text: ['why','is','this','so','complicated'],
}

export default TypedSpan;