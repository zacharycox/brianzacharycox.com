import React from 'react';
import PropTypes from "prop-types";

class SVGSprite extends React.Component {
  static propTypes = {
    name: PropTypes.oneOf(["arrow-dn", "arrow-left", "arrow", "burger"]),
  }

  static defaultProps = {
    name: '',
    width: '100%',
    height: '100%',
    className: 'svg-icon',
    viewBox: '0 0 20 20'
  }

  constructor(props) {
    super(props);
    this.name = props.name;
    this.width = props.width;
    this.height = props.height;
    this.className = `${props.className} ${this.getClassName(this)}`;
    this.svgViewBox = props.viewBox;
    this.path = this.getPath(this);
  }

  getPath() {
    let svgName = this.name;

    if (/arrow/.test(svgName)) {
      return <g id="arrow" style={{strokeLinecap: "square"}}>
        <line
          vectorEffect="non-scaling-stroke"
          x1="6.5" y1="2" x2="13.5" y2="10"
        />
        <line
          style={{strokeLinecap: "square"}}
          vectorEffect="non-scaling-stroke"
          x1="13.5" y1="10" x2="6.5" y2="18"
        />
      </g>;
    } else if (svgName === 'burger') {
      return <g id="burger" style={{strokeLinecap: "square"}}>
        <line id="burger-top"
              vectorEffect="non-scaling-stroke"
              x1="2" y1="4" x2="18" y2="4"
        />
        <line id="burger-middle"
              vectorEffect="non-scaling-stroke"
              x1="2" y1="10" x2="18" y2="10"
        />
        <line id="burger-bottom"
              vectorEffect="non-scaling-stroke"
              x1="2" y1="16" x2="18" y2="16"
        />
      </g>;
    } else {
      return 'No svg associated with that name.';
    }
  }

  getClassName() {
    switch(this.name) {
      case 'arrow-dn':
        return 'svg-icon__arrow--dn';
      case 'arrow-left':
        return 'svg-icon__arrow--left';
      case 'arrow':
        return 'svg-icon__arrow';
      case 'burger':
        return 'svg-icon__burger';
    }
  }

  // todo: build and pass props as object so attributes can be added to SVGLine as they
  // would be to a native svg element
  render() {
    return (
      <svg className={this.className} viewBox={this.svgViewBox} width={this.width} height={this.height}>
        {this.path}
      </svg>
    )
  }
}

export {SVGSprite}