import Link from 'gatsby-link';
import React from 'react';
import PropTypes from 'prop-types';
import {SVGSprite} from './svg-sprite.js';

// todo: put css classes in array

// Using class for access to lifecycle methods and state
class Nav extends React.Component {
  constructor(props) {
    super(props);
    this.getActiveLink = this.getActiveLink.bind(this);
    this.navData = this.props.navData.allFile.edges[0];
    this.navItems = this.navData.node.childMarkdownRemark.frontmatter.navItems;
    this.panels = this.navData.node.childMarkdownRemark.frontmatter.panels;
    this.state = {activeLink: null};
  }

  componentDidMount() {
    this.getActiveLink();
  }

  getActiveLink() {
    for (let navItem of this.navItems) {
      if (navItem.link) {
        let tester = new RegExp(navItem.link, "g");
        if (tester.test(window.location.href)) {
          this.setState({activeLink: navItem});
        }
      }
    }
    return false;
  }

  render() {
    return (
      <nav id={'mainNav'} className={'mainnav'}>
        <Navbar navItems={this.navItems} activeLink={this.state.activeLink}/>
        <Navdrawer panels={this.panels} activeLink={this.state.activeLink}/>
      </nav>
    )
  }
}

class Navdrawer extends React.Component {
  constructor(props) {
    super(props);
    this.activatePanel = this.activatePanel.bind(this);
    this.state = {
      isActive: false,
      activePanel: null,
    }
  }

  componentDidMount() {
    this.pageBody = document.getElementsByTagName('body')[0];
  }

  activatePanel(panel) {
    this.setState({activePanel: panel});
  }

  toggleNavdrawer() {
    if (this.state.isActive) {
      this.setState({
        isActive: false,
        activePanel: null
      });

      this.pageBody.classList.remove('navdrawer-active');
    } else {
      this.setState({
        isActive: true,
        activePanel: this.activePanel || 0
      })

      this.pageBody.classList.add('navdrawer-active');
    }
  }

  render() {
    const activeLink = this.props.activeLink;
    const panels = this.props.panels;

    return (
      <>
        <div className={'navdrawer'}>
          <div className={'navdrawer__trigger'} onClick={() => {this.toggleNavdrawer()}}>
            <SVGSprite name={'burger'}/>
          </div>
          <div className={'navdrawer__panel-viewport'}>
            <PanelContainer
              panels={panels}
              activeLink={activeLink}
              activePanel={this.state.activePanel}
              onActivatePanel={this.activatePanel}
            />
          </div>
        </div>
        <div
          className={(this.state.isActive) ? 'overlay--full-page is-active' : 'overlay--full-page'}
          onClick={(e) => {this.toggleNavdrawer(e)}}
        >
          <div className={'navdrawer__trigger'}>
            <SVGSprite name={'burger'}/>
          </div>
        </div>
      </>
    )
  }
}

class PanelContainer extends React.Component {
  constructor(props) {
    super(props);
    this.panels = this.props.panels;
  }

  render() {
    const panels = this.panels;
    const activeClassName = this.props.activePanel ? `navdrawer__panel-container lvl${this.props.activePanel.level}-active` : 'navdrawer__panel-container'

    return (
      <div className={activeClassName}>
        {panels.map((panel, index) => {
          return (
            <Panel
              key={index}
              panel={panel}
              panels={this.panels}
              activePanel={this.props.activePanel}
              onActivatePanel={this.props.onActivatePanel}
            />
          )
        })}
      </div>
    )
  }
}

class Panel extends React.Component {
  constructor(props) {
    super(props);
    this.panels = this.props.panels;
    this.panel = this.props.panel;
    this.title = this.panel.title;
    this.level = this.panel.level;
    this.items = this.panel.items;
    this.doActivateParent = this.doActivateParent.bind(this);
  }

  doActivateParent(parentTitle) {
    for(let panel of this.panels) {
      if (panel.title === parentTitle) {
        this.props.onActivatePanel(panel)
      }
    }
  }

  render() {
    const activeClassName = (this.props.activePanel === this.panel) ? `navdrawer__panel is-lvl${this.level} is-active` : `navdrawer__panel is-lvl${this.level}`;

    if (this.title !== 'main') {
      return (
        <div className={activeClassName}>
          <div className={'navdrawer__panel-header'}>
            <div className={'navdrawer__header-label'} onClick={() => {this.doActivateParent(this.panel.parent)}}>
              <SVGSprite name={'arrow-left'} />
              <span>about</span>
            </div>
          </div>
          {this.items.map((item, index) => {
            if (item.link) {
              return <NavdrawerItem key={index} navItem={item}/>
            } else {
              return (
                <NavdrawerPanelItem
                  key={index}
                  navItem={item}
                  panels={this.panels}
                  activePanel={this.props.activePanel}
                  onActivatePanel={this.props.onActivatePanel}
                />
              )
            }
          })}
        </div>
      )
    } else {
      return (
        <div className={activeClassName}>
          {this.items.map((item, index) => {
            if (item.link) {
              return <NavdrawerItem key={index} navItem={item}/>
            } else {
              return (
                <NavdrawerPanelItem
                  key={index}
                  navItem={item}
                  panels={this.panels}
                  activePanel={this.activePanel}
                  onActivatePanel={this.props.onActivatePanel}
                />
              )
            }
          })}
        </div>
      )
    }
  }
}

class NavdrawerItem extends React.Component {
  render() {
    const navItem = this.props.navItem;
    const activeLink = this.props.activeLink;
    const activeClassName = (navItem === activeLink) ? 'navdrawer__item-label is-active' : 'navdrawer__item-label';

    return (
      <div className={'navdrawer__item'}>
        <div className={activeClassName}>
          <a href={navItem.link}>{navItem.name}</a>
        </div>
      </div>
    )
  }
}

class NavdrawerPanelItem extends React.Component {
  constructor(props) {
    super(props);
    this.panels = this.props.panels;
    this.activePanel = this.props.activePanel;
    this.doActivatePanel = this.doActivatePanel.bind(this);
  }

  doActivatePanel(panelTitle) {
    for(let panel of this.panels) {
      if (panel.title === panelTitle) {
        this.props.onActivatePanel(panel)
      }
    }
  }

  render() {
    const navItem = this.props.navItem;
    const target = navItem.target;

    return (
      <div className={'navdrawer__item'}>
        <div className={'navdrawer__item-label has-subpanel'} onClick={() => {this.doActivatePanel(target)}}>
          <span>{navItem.name}</span>
          <SVGSprite name={'arrow'}/>
        </div>
      </div>
    )
  }
}

class Navbar extends React.Component {
  render() {
    const activeLink = this.props.activeLink;
    const navItems = this.props.navItems;

    return (
      <div className={'navbar'}>
        {navItems.map(function(navItem, index) {
          if (navItem.link) {
            return (
              <NavItem key={index} navItem={navItem} activeLink={activeLink} />
            )
          } else if (navItem.submenu) {
            return (
              <SubmenuItem key={index} navItem={navItem} />
            )
          }
        })}
      </div>
    )
  }
}

class NavItem extends React.Component {
  render() {
    const navItem = this.props.navItem;
    const activeLink = this.props.activeLink;
    const activeClassName = (navItem === activeLink) ? 'navbar__item-label is-active' : 'navbar__item-label';

    return (
      <div className={'navbar__item'}>
        <Link className={activeClassName} to={navItem.link}>{navItem.name}</Link>
      </div>
    )
  }
}

class SubmenuItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: false,
      isContentActive: false
    };
  }

  handleMouseover(e) {
    clearTimeout(this.timeout3);
    clearTimeout(this.timeout4);
    let dropdown = e.currentTarget;

    // Following acts as a 'hoverintent'
    this.timeout1 = setTimeout(() => {
      let n = dropdown.matches(':hover') || dropdown.querySelector(":hover");
      if (n) {
        this.setState({isActive: true});

        this.timeout2 = setTimeout(() => {
          this.setState({isContentActive: true});
        }, 300)
      }
    }, 200);
  }

  handleMouseout(e) {
    clearTimeout(this.timeout1);
    clearTimeout(this.timeout2);
    let dropdown = e.currentTarget;

    // Following acts as a 'hoverintent'
    this.timeout3 = setTimeout(() => {
      let n = dropdown.matches(':hover') || dropdown.querySelector(":hover");
      if (!n && n == null) {
        this.setState({isContentActive: false});

        this.timeout4 = setTimeout(() => {
          this.setState({isActive: false});
        }, 300)
      }
    }, 200);
  }

  componentWillUnmount() {
    clearInterval(this.timeout1);
    clearInterval(this.timeout2);
    clearInterval(this.timeout3);
    clearInterval(this.timeout4);
  }

  render() {
    const navItem = this.props.navItem;
    const activeClassName = (this.state.isActive) ? 'navbar__item is-active' : 'navbar__item';

    return (
      <div className={activeClassName}
           onMouseOver={(e) => {this.handleMouseover(e)}}
           onMouseOut={(e) => {this.handleMouseout(e)}}
      >
        <div className={'navbar__item-label has-dropdown'}>
          <span>{navItem.name}</span>
          <SVGSprite name={'arrow-dn'}/>
        </div>
        <Submenu
          submenu={navItem.submenu}
          isActive={this.state.isActive}
          isContentActive={this.state.isContentActive}
        />
      </div>
    )
  }
}

// todo: make this recursive to handle addtl submenu lvls
class Submenu extends React.Component {
  render() {
    const submenu = this.props.submenu;
    const isActive = this.props.isActive;
    const isContentActive = this.props.isContentActive;
    const activeClassName = isActive ? 'dropdown__content-wrap is-active' : 'dropdown__content-wrap';
    const activeContentClassName = isContentActive ? 'dropdown__content is-active' : 'dropdown__content';

    return (
      <div className={activeClassName}>
        <div className={activeContentClassName}>
          <div className={'container'}>
            {submenu.map(function (submenuItem, index) {
              return (
                submenuItem.link &&
                <div key={index}>
                  <Link to={submenuItem.link}>{submenuItem.name}</Link>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    )
  }
}

export default Nav;