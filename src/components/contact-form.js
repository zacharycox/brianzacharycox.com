import React from 'react';
import FeatureButton from "./feature-button";
import Validation from "./validation";

export default class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.resetForm = this.resetForm.bind(this);
  }

  state = {
    firstName: '',
    lastName: '',
    company: '',
    email: '',
    phone: '',
    message: '',
    touched: {
      firstName: false,
      lastName: false,
      email: false,
      message: false,
    }
  };

  handleSubmit() {
    const name = `${this.state.firstName} ${this.state.lastName}`;
    const message = `<h1>From: ${name}</h1>` +
      `<h3>Company: ${this.state.company}</h3>` +
      `<h3>Email: ${this.state.email}</h3>` +
      `<h3>Phone: ${this.state.phone}</h3>` +
      `<h3>Message:</h3>` +
      `<p>${this.state.message}</p>`;

    const body = {
      toEmails: ['brianzacharycox@gmail.com'],
      message: message,
      subject: `BZC Contact: ${name}`
    };

    fetch('https://9v5sv9s8x4.execute-api.us-east-1.amazonaws.com/default/sendMail', {
      method: 'post',
      body: JSON.stringify(body),
      headers: {'Content-Type': 'application/json'}
    }).then(function(res) { return res.json(); })
      .then(function(resjson) { alert("res: " + JSON.stringify(resjson)) });
  }

  resetForm(){
    this.setState({
      firstName: '',
      lastName: '',
      company: '',
      email: '',
      phone: '',
      subject: '',
      message: '',
      touched: {
        firstName: false,
        lastName: false,
        email: false,
        message: false,
      }
    })
  }

  handleInputChange(e) {
    const changedInput = e.target;
    const value = changedInput.value;
    const name = changedInput.name;

    this.setState({
      [name]: value,
    })
  }

  handleBlur = (e) => {
    this.setState({
      touched: { ...this.state.touched, [e.target.name]: true },
    });
  }

  render() {
    const formValidation = new Validation({
        firstName : {
          v: this.state.firstName,
          tests: ['notEmpty']
        },
        lastName : {
          v: this.state.lastName,
          tests: ['notEmpty']
        },
        email : {
          v: this.state.email,
          tests: ['notEmpty', 'isEmail']
        },
        phone : {
          v: this.state.phone,
          tests: ['isPhone']
        },
        message : {
          v: this.state.message,
          tests: ['notEmpty']
        },
      }).result;

    const showErr = (field) => {
        const hasError = formValidation[field] !== undefined && formValidation[field] !== '';
        const shouldShow = this.state.touched[field];
        return hasError ? shouldShow : false;
      };

    return (
      <form className={'contactForm section__content'} id={'contact'} action={'/success'}>
        <div className={'contactForm__body'}>
          <div className={'contactForm__row'}>
            <p className="hidden">
              <label>Don’t fill this out if you're human: <input name="bot-field"/></label>
            </p>
            <div className={showErr('firstName') ? "contactForm__input--reqd error" : "contactForm__input--reqd"}>
              <label htmlFor={'contactFname'}>First name</label>
              <input
                id={'contactFname'}
                type={'text'}
                name={'firstName'}
                value={this.state.firstName}
                onChange={this.handleInputChange}
                onBlur={this.handleBlur}
              />
              <div className={'validationError'}>{showErr('firstName') ? formValidation['firstName'] : ''}</div>
            </div>
            <div className={showErr('lastName') ? "contactForm__input--reqd error" : "contactForm__input--reqd"}>
              <label htmlFor={'contactLname'}>Last name</label>
              <input
                id={'contactLname'}
                type={'text'}
                name={'lastName'}
                value={this.state.lastName}
                onChange={this.handleInputChange}
                onBlur={this.handleBlur}
              />
              <div className={'validationError'}>{showErr('lastName') ? formValidation['lastName'] : ''}</div>
            </div>
          </div>
          <div className={'contactForm__row'}>
            <div className={showErr('email') ? "contactForm__input--reqd error" : "contactForm__input--reqd"}>
              <label htmlFor={'contactEmail'}>Email</label>
              <input
                id={'contactEmail'}
                type={'text'}
                name={'email'}
                value={this.state.email}
                onChange={this.handleInputChange}
                onBlur={this.handleBlur}
              />
              <div className={'validationError'}>{showErr('email') ? formValidation['email'] : ''}</div>
            </div>
          </div>
          <div className={'contactForm__row'}>
            <div className={showErr('phone') ? "contactForm__input error" : "contactForm__input"}>
              <label htmlFor={'contactPhone'}>Phone</label>
              <input
                id={'contactPhone'}
                type={'text'}
                name={'phone'}
                value={this.state.phone}
                onChange={this.handleInputChange}
                onBlur={this.handleBlur}
              />
              <div className={'validationError'}>{showErr('phone') ? formValidation['phone'] : ''}</div>
            </div>
          </div>
          <div className={'contactForm__row'}>
            <div className={'contactForm__input'}>
              <label htmlFor={'contactCompany'}>Company</label>
              <input
                id={'contactCompany'}
                type={'text'}
                name={'company'}
                value={this.state.company}
                onChange={this.handleInputChange}
                onBlur={this.handleBlur}
              />
            </div>
          </div>
          <div className={'contactForm__row'}>
            <div className={showErr('message') ? "contactForm__input--reqd error" : "contactForm__input--reqd"}>
              <label htmlFor={'contactMessage'}>Message</label>
              <textarea
                rows={'5'}
                id={'contactMessage'}
                type={'textarea'}
                name={'message'}
                value={this.state.message}
                onChange={this.handleInputChange}
                onBlur={this.handleBlur}
              />
              <div className={'validationError'}>{showErr('message') ? formValidation['message'] : ''}</div>
            </div>
          </div>
        </div>
          <FeatureButton cssClass={'content-cta'} actionType={'sbmt'} action={this.handleSubmit} text={'Send message'} isDisabled={(Object.keys(formValidation).length > 0)}/>
      </form>
    )
  }
}