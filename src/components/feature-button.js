import React from 'react';
import PropTypes from 'prop-types';
import {navigate} from 'gatsby';
import {executeFunctionByName} from 'helpers';
// import featureButtonStyles from "../styles/components/feature-button.module.scss";

export default class FeatureButton extends React.Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    if (this.props.actionType === 'nav') {
      navigate(this.props.action);
    } else if (this.props.actionType === 'extnav') {
      window.open(this.props.action, '_blank');
    } else if (this.props.actionType === 'sbmt') {
      this.props.action();
    } else {
      executeFunctionByName(this.props.action, this);
    }
  }

  render() {
    const isDisabled = this.props.isDisabled ? 'is-disabled' : '';
    return (
      <button className={`featureButton ${this.props.cssClass} is-${this.props.size} is-${this.props.color} ${isDisabled}`} onClick={this.handleClick} disabled={isDisabled}>
        <span className={'featureButton__text'}>{this.props.text}</span>
      </button>
    );
  }
}

FeatureButton.propTypes = {
  cssClass: PropTypes.string,
  text: PropTypes.string,
  size: PropTypes.oneOf(["small", "medium", "large"]),
  color: PropTypes.oneOf(["white", "black", "gray"]),
  actionType: PropTypes.oneOf(["nav", "extnav", "sbmt", "fxn"]),
  action: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  isDisabled: PropTypes.bool,
}

FeatureButton.defaultProps = {
  cssClass: "",
  text: "click",
  size: "medium",
  color: "black",
  actionType: "nav",
  action: "/",
  isDisabled: false,
}