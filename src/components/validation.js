
export default class Validation {
  constructor(validationObj) {
    this.validationObj = validationObj;
    this.getResult = this.getResult.bind(this);
    this.result = this.getResult();
  }

  getResult() {
    let returnObj = {};
    Object.keys(this.validationObj).map((fieldName) => {
      let field = fieldName;
      let value = this.validationObj[fieldName]['v'];
      let tests = this.validationObj[fieldName]['tests'];

      if (tests.includes('notEmpty')) {
        for (let test of tests) {
          if (!ValidationMethods[test].test(value)) {
            returnObj[field] = ValidationMethods[test].errMsg;
            break;
          }
        }
      } else {
        for (let test of tests) {
          if (ValidationMethods['notEmpty'].test(value) && !ValidationMethods[test].test(value)) {
            returnObj[field] = ValidationMethods[test].errMsg;
            break;
          }
        }
      }
    });

    return returnObj;
  }
}

// NEGATIVE tests - return false if value fails validation
const ValidationMethods = {
  notEmpty: {
    test: function (v) {
      return (v !== '' && (v != null) && (v.length !== 0) && !(/^\s+$/.test(v)));
    },
    errMsg: 'This is a required field.'
  },
  isNumber: {
    test: function (v) {
      return !isNaN(parseNumber(v)) && /^\s*-?\d*(\.\d*)?\s*$/.test(v)
    },
    errMsg: 'Must be a number.'
  },
  isEmail: {
    test: function (v) {
      return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v)
    },
    errMsg: 'Please enter a valid email address.'
  },
  isPhone: {
    test: function (v) {
      return /^(((\(\d{3}\))|\d{3}))?[-. ]?\d{3}[-. ]?\d{4}$/.test(v);
    },
    errMsg: 'Please enter a valid phone number.'
  }
};

function parseNumber(v) {
  if (typeof v !== 'string') {
    return parseFloat(v);
  }

  let isDot = v.indexOf('.');
  let isComma = v.indexOf(',');

  if (isDot !== -1 && isComma !== -1) {
    if (isComma > isDot) {
      v = v.replace('.', '').replace(',', '.');
    } else {
      v = v.replace(',', '');
    }
  } else if (isComma !== -1) {
    v = v.replace(',', '.');
  }

  return parseFloat(v);
}
