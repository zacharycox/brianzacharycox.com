import React from 'react';
import logo from '../images/bzc-logo-11.svg';

const Header = () => (
  <section id={'staticHeader'} className={'section hdr'}>
    <div className={'hdr__outer-wrap'}>
      <div className={'hdr__lt-wrap'}>
        <div className={'hdr__lt'}>
          <div className={'hdr__logo'}>
            <a href={'/'}>
              <img src={logo} alt={'Developer'}/>
            </a>
          </div>
        </div>
      </div>
      <div className={'hdr__rt-wrap'}>
        <div className={'hdr__rt'}>
          <div className={'nav__item'}>
            <a href={""}>work</a>
          </div>
          <div className={'nav__item'}>
            <a href={""}>about</a>
          </div>
          <div className={'nav__item'}>
            <a href={""}>resume</a>
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default Header
