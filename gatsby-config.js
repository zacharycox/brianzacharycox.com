module.exports = {
  siteMetadata: {
    title: 'Zax CV',
    description: 'A single-page web application showcasing the development talents of Brian Zachary Cox',
    author: 'Brian Zachary Cox',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/src/data`,
      },
    },
    'gatsby-transformer-remark',
    'gatsby-plugin-sass',
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        icon: 'src/images/bzc-logo-11.svg', // This path is relative to the root of the site.
      },
    },
    {
      resolve: '@fs/gatsby-plugin-drive',
      options: {
        folderId: '1-nStyUg0FZLYbEQ3Jm4HCYn_tPIJPHHq',
        keyFile: `${__dirname}/client_secret.json`,
        destination: `${__dirname}/src/assets`,
        exportGDocs: true,
        exportMimeType: 'text/html'
      }
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    // 'gatsby-plugin-offline',
  ],
}
