/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const path = require('path');
const devMode = process.env.NODE_ENV !== "production";

console.log('HELLO');
console.log(path.resolve(__dirname, "js"));
console.log('GOODBYE');


exports.onCreateWebpackConfig = ({
  stage,
  rules,
  loaders,
  plugins,
  actions,
}) => {
  actions.setWebpackConfig({
    module: {
      rules: [
        {
          test: /\.js$/,
          include: [
            path.resolve(__dirname, "js/modules"),
            path.resolve(__dirname, "js/utils")
          ],
          exclude: [
            path.resolve(__dirname, "node_modules")
          ],
          use: {
            loader: "babel-loader",
            options: {
              presets: [
                [
                  "env",
                  {
                    // Prevent Babel from compiling ES6 modules back to CommonJS, as webpack
                    // will take care of that itself. This allows for tree-shaking and thus a
                    // smaller output bundle size. See:
                    // https://developers.google.com/web/fundamentals/performance/optimizing-javascript/tree-shaking/
                    // "modules": false,
                    "modules": true,
                    // Specify browser compatibility to target (based on caniuse.com stats).
                    "targets": {
                      "browsers": "> 1% in US"
                    },
                    // This causes the matched browser versions & features to be output.
                    // Good to leave in place for diagnostics.
                    "debug": true
                  }
                ]
              ]
            }
          },
        },
      ]
    },
    resolve: {
      modules: [
        "src/js/modules",
        "src/js/utils",
        "node_modules",
        "css"
      ]
    },
    devtool: devMode ? "eval-source-map" : false,
    target: "web"
  });
};